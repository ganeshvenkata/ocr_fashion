view: amazon_parent {
  label: "Amazon Parent"
  derived_table: {
    sql: select
        distinct
           parent_asin as asin,
          tld,
          week_beginning
        from amz_parent_child  ;;
  }

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: concat(${ASIN}, ${TLD}, ${week_beginning_raw}) ;;
  }


  dimension: ASIN {
    type: string
    sql: ${TABLE}.ASIN ;;
    view_label: "Dimensions" #First dropdown
    label: "PARENT ASINs"
    description: ""
    group_label: "Amazon Parent Child"
  }
  dimension: TLD {
    type: string
    sql: ${TABLE}.TLD ;;
    view_label: "Dimensions" #First dropdown
    label: "TLD"
    description: ""
    group_label: "Amazon Parent Child"
  }
  dimension_group: week_beginning {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.WEEK_BEGINNING ;;
    view_label: "Dimensions" #First dropdown
    label: "Week Beginning - "
    description: ""
    group_label: "Time Frame"
  }
  measure: count {
    type: count
    hidden: yes
    drill_fields: [ASIN, week_beginning_date]
  }

}

view: amazon_child {
  label: "Amazon Parent Child"
  derived_table: {
    sql: select
        distinct asin,
          tld,
          week_beginning
        from amz_parent_child  ;;
  }

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: concat(${ASIN}, ${TLD}, ${week_beginning_raw}) ;;
  }



  dimension: ASIN {
    type: string
    sql: ${TABLE}.ASIN ;;
    view_label: "Dimensions" #First dropdown
    label: "CHILD ASINs"
    description: ""
    group_label: "Amazon Parent Child"
  }
  dimension: TLD {
    type: string
    sql: ${TABLE}.TLD ;;
    view_label: "Dimensions" #First dropdown
    label: "TLD"
    description: ""
    group_label: "Amazon Parent Child"
  }
  dimension_group: week_beginning {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.WEEK_BEGINNING ;;
    view_label: "Dimensions" #First dropdown
    label: "Week Beginning - "
    description: ""
    group_label: "Time Frame"
  }
  measure: count {
    type: count
    hidden: yes
    drill_fields: [ASIN, week_beginning_date]
  }

}
