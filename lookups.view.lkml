view: week_beginning_lookups {
  derived_table: {
    sql:  select distinct week_beginning from fashion_data order by 1 ;;
    persist_for: "24 hours"
  }

  dimension_group: week_beginning {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.WEEK_BEGINNING ;;
    view_label: "Dimensions" #First dropdown
    label: "Week Beginning - "
    description: ""
    group_label: "Time Frame"
  }

}

view: manufacturer_lookups {
  derived_table: {
    sql:  select distinct manufacturer from catalog where client_id=660  order by 1;;

  }

  dimension: manufacturer {
    type: string
    sql: ${TABLE}.manufacturer ;;
  }
}

view: brand_lookups {
  derived_table: {
    sql:  select distinct brand from catalog where client_id=660 order by 1;;

  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }
}

view: category_lookups {
  derived_table: {
    sql:  select distinct category from catalog where client_id=660 order by 1;;

  }

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }
}

view: sub_category_lookups {
  derived_table: {
    sql:  select distinct subcategory from catalog  where client_id=660 order by 1;;

  }

  dimension: subcategory {
    type: string
    sql: ${TABLE}.subcategory ;;
  }
}
view: client_product_group_lookups {
  derived_table: {
    sql:  select distinct client_product_group  from catalog where client_id=660 order by 1;;

  }

  dimension: client_product_group {
    type: string
    sql: ${TABLE}.client_product_group ;;
  }
}

view: parent_asin_lookups {
  derived_table: {
    sql:  select distinct parent_asin as asin from amz_parent_child order by 1;;

  }

  dimension: asin {
    type: string
    sql: ${TABLE}.asin ;;
  }
}



view: child_asin_lookups {
  derived_table: {
    sql:  select distinct asin as asin from amz_parent_child order by 1;;
    persist_for: "24 hours"
  }

  dimension: asin {
    type: string
    sql: ${TABLE}.asin ;;
  }
}

view: asin_title_lookups {
  derived_table: {
    sql:  select distinct asin || ' - ' || title as asin_title from catalog order by 1;;

  }

  dimension: asin_title {
    type: string
    sql: ${TABLE}.asin_title ;;
  }
}
