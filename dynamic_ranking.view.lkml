view: dynamic_ranking_parent {
  view_label: "Top N Filter"
  derived_table: {
    sql:
     WITH
       base AS
        (
          SELECT
            {% if _filters['dimension_name_selector'] == 'Manufacturer' %} catalog.Manufacturer
            {% elsif _filters['dimension_name_selector'] == 'Brand' %} catalog.brand
            {% elsif _filters['dimension_name_selector'] == 'Category' %} catalog.category
            {% elsif _filters['dimension_name_selector'] == 'Sub Category' %} catalog.SubCategory
            {% elsif _filters['dimension_name_selector'] == 'Client Product Group' %} catalog.Client_Product_Group
            {% elsif _filters['dimension_name_selector'] == 'ASIN' %} amazon_parent.ASIN
            {% elsif _filters['dimension_name_selector'] == 'ASIN Title' %} catalog.asin || ' - ' || catalog.title
            {% endif %} as dimension_name,
            RANK () OVER ( ORDER BY
              {% if _filters['measure_name_selector'] == 'Total Sales' %} sum(total_sales)
              {% elsif _filters['measure_name_selector'] == 'Total Units' %} sum(total_units)
              {% endif %} DESC) as rank
            FROM catalog  AS catalog
            JOIN PUBLIC.CLIENTS  AS client ON catalog.Client_Id=client.Id
            JOIN PUBLIC.FASHION_DATA  AS fashion_data ON fashion_data.ASIN=catalog.ASIN
            JOIN amazon_parent  AS amazon_parent ON fashion_data.ASIN = amazon_parent.ASIN
             AND amazon_parent.TLD=client.TLD
             AND amazon_parent.WEEK_BEGINNING = fashion_data.WEEK_BEGINNING
           WHERE
              client.id = 660
              AND {% condition week_beginning_filter %} fashion_data.WEEK_BEGINNING {% endcondition %}
              AND {% condition manufacturer_filter %} catalog.manufacturer {% endcondition %}
              AND {% condition brand_filter %} catalog.brand {% endcondition %}
              AND {% condition category_filter %} catalog.category {% endcondition %}
              AND {% condition sub_category_filter %} catalog.subcategory {% endcondition %}
              AND {% condition client_product_group_filter %} catalog.client_product_group {% endcondition %}
              AND {% condition parent_asin_filter %} amazon_parent.ASIN {% endcondition %}
              AND {% condition asin_title_filter %} catalog.asin || ' - ' || catalog.title {% endcondition %}
            GROUP BY 1
            HAVING
             {% if _filters['measure_name_selector'] == 'Total Sales' %} sum(total_sales)
             {% elsif _filters['measure_name_selector'] == 'Total Units' %} sum(total_units)
             {% endif %} > 0
          )


     select
       base.dimension_name AS dimension_name,
      rank
    from base
    where  rank<={% parameter top_n_selector %}
    ;;

    }

    parameter: dimension_name_selector {
      description: "Name of Dimension for Y Axis. Required Field"
      type: string
      view_label: "1 - Selector" #First dropdown
      label: "Show Dimension"
      allowed_value: {
        label: "Manufacturer"
        value: "Manufacturer"
      }
      allowed_value: {
        label: "Brand"
        value: "Brand"
      }
      allowed_value: {
        label: "Category"
        value: "Category"
      }
      allowed_value: {
        label: "Sub Category"
        value: "Sub Category"
      }
      allowed_value: {
        label: "Client Product Group"
        value: "Client Product Group"
      }
      allowed_value: {
        label: "ASIN"
        value: "ASIN"
      }
      allowed_value: {
        label: "ASIN Title"
        value: "ASIN Title"
      }
    }

    parameter: measure_name_selector {
      description: "Name of Measure for Y Axis value. Required Field"
      view_label: "1 - Selector" #First dropdown
      label: "By Measure"
      type: string
      allowed_value: {
        label: "Total Sales"
        value: "Total Sales"
      }
      allowed_value: {
        label: "Total Units"
        value: "Total Units"
      }
    }

    filter: week_beginning_filter {

      view_label: "1 - Selector" #First dropdown
      label: "In Last X timeframe"
      type: date
      suggest_explore: week_beginning_lookups
      suggest_dimension: week_beginning_lookups.week_beginning
    }
    filter: manufacturer_filter {
      view_label: "Filters"
      type: string
      suggest_explore: manufacturer_lookups
      suggest_dimension: manufacturer_lookups.manufacturer
    }
    filter: sub_category_filter {
      view_label: "Filters"
      type: string
      suggest_explore: sub_category_lookups
      suggest_dimension: sub_category_lookups.subcategory
    }
    filter: brand_filter {
      view_label: "Filters"
      type: string
      suggest_explore: brand_lookups
      suggest_dimension: brand_lookups.brand
    }
    filter: category_filter {
      view_label: "Filters"
      type: string
      suggest_explore: category_lookups
      suggest_dimension: category_lookups.category
    }
    filter: client_product_group_filter {
      view_label: "Filters"
      type: string
      suggest_explore: client_product_group_lookups
      suggest_dimension: client_product_group_lookups.client_product_group
    }
    filter: parent_asin_filter {
      view_label: "Filters"
      type: string
      suggest_explore: parent_asin_lookups
      suggest_dimension: parent_asin_lookups.asin
    }
    filter: asin_title_filter {
      view_label: "Filters"
      type: string
      suggest_explore: asin_title_lookups
      suggest_dimension: asin_title_lookups.asin
    }

  parameter: show_all_others{
    label: "Show All Others"
    description:"Show All Others (Don't use while doing share). Default is Yes"
    view_label: "1 - Selector" #First dropdown
    type:  string
    default_value: "Yes"
    allowed_value: {
      label: "Yes"
      value: "Yes"
    }
    allowed_value: {
      label: "No"
      value: "No"
    }
  }

    parameter: top_n_selector{
      label: "Top N Filter"
      description:" Enter number for Top N. Default 10"
      view_label: "1 - Selector" #First dropdown

      type:  number
      default_value: "10"
      allowed_value: {
        label: "5"
        value: "5"
      }
      allowed_value: {
        label: "10"
        value: "10"
      }
      allowed_value: {
        label: "15"
        value: "15"
      }
      allowed_value: {
        label: "20"
        value: "20"
      }
      allowed_value: {
        label: "25"
        value: "25"
      }
      allowed_value: {
        label: "50"
        value: "50"
      }

    }

    dimension: Dimension_name {
      hidden: yes
      type: string
      sql:IFNULL(${TABLE}.dimension_name,'-All Others-') ;;
    }
  dimension: Dimension_name_null {
    hidden: yes
    type: string
    sql:${TABLE}.dimension_name ;;
  }
    dimension: Rank {
      type: number
      sql: ${TABLE}.rank ;;
      view_label: "Dimensions" #First dropdown
      label: "Rank"

    }
    dimension: display_dimension {
      view_label: "Dimensions" #First dropdown
      label: "Dimension"
      label_from_parameter: dynamic_ranking_parent.dimension_name_selector
      sql: ${Dimension_name} ;;
    }


  }


view: dynamic_ranking_child {
  view_label: "Top N Filter"
  derived_table: {
    sql:
     WITH
       base AS
        (
          SELECT
            {% if _filters['dimension_name_selector'] == 'Manufacturer' %} catalog.Manufacturer
            {% elsif _filters['dimension_name_selector'] == 'Brand' %} catalog.brand
            {% elsif _filters['dimension_name_selector'] == 'Category' %} catalog.category
            {% elsif _filters['dimension_name_selector'] == 'Sub Category' %} catalog.SubCategory
            {% elsif _filters['dimension_name_selector'] == 'Client Product Group' %} catalog.Client_Product_Group
            {% elsif _filters['dimension_name_selector'] == 'ASIN' %} amazon_child.ASIN
            {% elsif _filters['dimension_name_selector'] == 'ASIN Title' %} catalog.asin || ' - ' || catalog.title
            {% endif %} as dimension_name,
            RANK () OVER ( ORDER BY
              {% if _filters['measure_name_selector'] == 'Total Sales' %} sum(total_sales)
              {% elsif _filters['measure_name_selector'] == 'Total Units' %} sum(total_units)
              {% endif %} DESC) as rank
            FROM PUBLIC.Catalog  AS catalog
            JOIN PUBLIC.CLIENTS  AS client ON catalog.Client_Id=client.Id
            JOIN PUBLIC.FASHION_DATA  AS fashion_data ON fashion_data.ASIN=catalog.ASIN
            JOIN amazon_child  AS amazon_child ON fashion_data.ASIN = amazon_child.ASIN
            AND amazon_child.TLD=client.TLD
            AND amazon_child.WEEK_BEGINNING = fashion_data.WEEK_BEGINNING
            AND client.Manufacturer_NAME = catalog.Manufacturer
           WHERE
              client.id = 660
              AND {% condition week_beginning_filter %} fashion_data.WEEK_BEGINNING {% endcondition %}
              AND {% condition manufacturer_filter %} catalog.manufacturer {% endcondition %}
              AND {% condition brand_filter %} catalog.brand {% endcondition %}
              AND {% condition category_filter %} catalog.category {% endcondition %}
              AND {% condition sub_category_filter %} catalog.subcategory {% endcondition %}
              AND {% condition client_product_group_filter %} catalog.client_product_group {% endcondition %}
              AND {% condition child_asin_filter %} amazon_parent_child.ASIN {% endcondition %}
              AND {% condition asin_title_filter %} catalog.asin || ' - ' || catalog.title {% endcondition %}
            GROUP BY 1
            HAVING
             {% if _filters['measure_name_selector'] == 'Total Sales' %} sum(total_sales)
             {% elsif _filters['measure_name_selector'] == 'Total Units' %} sum(total_units)
             {% endif %} > 0
          )


     select
      base.dimension_name AS dimension_name, rank
    from base
    where  rank<={% parameter top_n_selector %}
    ;;

    }

  parameter: show_all_others{
    label: "Show All Others"
    description:"Show All Others (Don't use while doing share). Default is Yes"
    view_label: "1 - Selector" #First dropdown
    type:  string
    default_value: "Yes"
    allowed_value: {
      label: "Yes"
      value: "Yes"
    }
    allowed_value: {
      label: "No"
      value: "No"
    }
  }

    parameter: dimension_name_selector {
      description: "Name of Dimension for Y Axis. Required Field"
      type: string
      view_label: "1 - Selector" #First dropdown
      label: "Show Dimension"
      allowed_value: {
        label: "Manufacturer"
        value: "Manufacturer"
      }
      allowed_value: {
        label: "Brand"
        value: "Brand"
      }
      allowed_value: {
        label: "Category"
        value: "Category"
      }
      allowed_value: {
        label: "Sub Category"
        value: "Sub Category"
      }
      allowed_value: {
        label: "Client Product Group"
        value: "Client Product Group"
      }
      allowed_value: {
        label: "ASIN"
        value: "ASIN"
      }
      allowed_value: {
        label: "ASIN Title"
        value: "ASIN Title"
      }
    }

    parameter: measure_name_selector {
      description: "Name of Measure for Y Axis value. Required Field"
      view_label: "1 - Selector" #First dropdown
      label: "By Measure"
      type: string
      allowed_value: {
        label: "Total Sales"
        value: "Total Sales"
      }
      allowed_value: {
        label: "Total Units"
        value: "Total Units"
      }
    }

    filter: week_beginning_filter {

      view_label: "1 - Selector" #First dropdown
      label: "In Last X timeframe"
      type: date
      suggest_explore: week_beginning_lookups
      suggest_dimension: week_beginning_lookups.week_beginning
    }
    filter: manufacturer_filter {
      view_label: "Filters"
      type: string
      suggest_explore: manufacturer_lookups
      suggest_dimension: manufacturer_lookups.manufacturer
    }
    filter: sub_category_filter {
      view_label: "Filters"
      type: string
      suggest_explore: sub_category_lookups
      suggest_dimension: sub_category_lookups.subcategory
    }
    filter: brand_filter {
      view_label: "Filters"
      type: string
      suggest_explore: brand_lookups
      suggest_dimension: brand_lookups.brand
    }
    filter: category_filter {
      view_label: "Filters"
      type: string
      suggest_explore: category_lookups
      suggest_dimension: category_lookups.category
    }
    filter: client_product_group_filter {
      view_label: "Filters"
      type: string
      suggest_explore: client_product_group_lookups
      suggest_dimension: client_product_group_lookups.client_product_group
    }
    filter: child_asin_filter {
      view_label: "Filters"
      type: string
      suggest_explore: child_asin_lookups
      suggest_dimension: child_asin_lookups.asin
    }
    filter: asin_title_filter {
      view_label: "Filters"
      type: string
      suggest_explore: asin_title_lookups
      suggest_dimension: asin_title_lookups.asin_title
    }

    parameter: top_n_selector{
      label: "Top N Filter"
      description:" Enter number for Top N. Default 10"
      view_label: "1 - Selector" #First dropdown

      type:  number
      default_value: "10"
      allowed_value: {
        label: "5"
        value: "5"
      }
      allowed_value: {
        label: "10"
        value: "10"
      }
      allowed_value: {
        label: "15"
        value: "15"
      }
      allowed_value: {
        label: "20"
        value: "20"
      }
      allowed_value: {
        label: "25"
        value: "25"
      }
      allowed_value: {
        label: "50"
        value: "50"
      }

    }

  dimension: Dimension_name {
    hidden: yes
    type: string
    sql:IFNULL(${TABLE}.dimension_name,'-All Others-') ;;
  }
  dimension: Dimension_name_null {
    hidden: yes
    type: string
    sql:${TABLE}.dimension_name ;;
  }

    dimension: Rank {
      type: number
      sql: ${TABLE}.rank ;;
      view_label: "Dimensions" #First dropdown
      label: "Rank"
    }
    dimension: display_dimension {
      view_label: "Dimensions" #First dropdown
      label: "Dimension"
      label_from_parameter: dynamic_ranking_child.dimension_name_selector
      sql: ${Dimension_name} ;;
    }


  }
