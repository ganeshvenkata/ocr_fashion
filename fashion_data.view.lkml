view: fashion_data
{

label: "Fashion Data"
sql_table_name: PUBLIC.FASHION_DATA ;;

dimension: ASIN {
  type: string
  sql: ${TABLE}.ASIN ;;
  label: "ASIN"
  hidden: yes
}
  dimension_group: week_beginning {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.WEEK_BEGINNING ;;
    view_label: "Dimensions" #First dropdown
    label: "Week Beginning - "
    description: ""
    group_label: "Time Frame"
    hidden: yes
  }
  dimension: _totalUnits {
    type: number
    sql: ${TABLE}.TOTAL_UNITS ;;
    hidden: yes
  }
  dimension: _1PUnits {
    type: number
    sql: ${TABLE}.Units_1P ;;
    hidden: yes
  }
  dimension: _3PUnits {
    type: number
    sql: ${TABLE}.Units_3P ;;
    hidden: yes
  }

  dimension: _totalPrice {
    type: number
    sql: ${TABLE}.TOTAL_Price ;;
    hidden: yes
  }
  dimension: _1PPrice {
    type: number
    sql: ${TABLE}.Price_1P ;;
    hidden: yes
  }
  dimension: _3PPrice {
    type: number
    sql: ${TABLE}.Price_3P ;;
    hidden: yes
  }

  dimension: _totalSales {
    type: number
    sql: ${TABLE}.TOTAL_Sales ;;
    hidden: yes

  }
  dimension: _1PSales {
    type: number
    sql: ${TABLE}.Sales_1P ;;
    hidden: yes
  }
  dimension: _3PSales {
    type: number
    sql: ${TABLE}.Sales_3P ;;
    hidden: yes
  }

  dimension: _per_1p {
    type: number
    sql: ${TABLE}.per_1p ;;
    hidden: yes
  }

  measure: TotalUnits {
    type:  sum
    sql: ${_totalUnits} ;;
    view_label: "Measures" #First dropdown
    group_label: "Total Metrics" #Second dropdown
    label: "Total Units"
    description: ""
  }

  measure: TotalPrice {
    type:  sum
    sql: ${_totalPrice} ;;
    view_label: "Measures" #First dropdown
    group_label: "Total Metrics" #Second dropdown
    label: "Total Price"
    description: ""
  }
  measure: TotalSales {
    type:  sum
    sql: ${_totalSales} ;;
    view_label: "Measures" #First dropdown
    group_label: "Total Metrics" #Second dropdown
    label: "Total Sales"
    description: ""
    value_format_name: auto
  }
  measure: Total1PUnits {
    type:  sum
    sql: ${_1PUnits} ;;
    view_label: "Measures" #First dropdown
    group_label: "1P Metrics" #Second dropdown
    label: "Total 1P Units"
    description: ""
  }

  measure: Total1PPrice {
    type:  sum
    sql: ${_1PUnits} ;;
    view_label: "Measures" #First dropdown
    group_label: "1P Metrics" #Second dropdown
    label: "Total 1P Price"
    description: ""
  }
  measure: Total1PSales {
    type:  sum
    sql: ${_1PSales} ;;
    view_label: "Measures" #First dropdown
    group_label: "1P Metrics" #Second dropdown
    label: "Total 1P Sales"
    description: ""
  }

  measure: Total3PUnits {
    type:  sum
    sql: ${_3PUnits} ;;
    view_label: "Measures" #First dropdown
    group_label: "3P Metrics" #Second dropdown
    label: "Total 3P Units"
    description: ""
  }

  measure: Total3PPrice {
    type:  sum
    sql: ${_3PUnits} ;;
    view_label: "Measures" #First dropdown
    group_label: "3P Metrics" #Second dropdown
    label: "Total 3P Price"
    description: ""
  }
  measure: Total3PSales {
    type:  sum
    sql: ${_3PSales} ;;
    view_label: "Measures" #First dropdown
    group_label: "3P Metrics" #Second dropdown
    label: "Total 3P Sales"
    description: ""
  }

}
