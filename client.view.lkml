view: client {

  label: "Client"
  sql_table_name: PUBLIC.CLIENTS ;;

  dimension: ClientId {
    type: number
    sql: ${TABLE}.Id ;;
    primary_key: yes
    hidden: yes

  }

  dimension: TLD {
    type: string
    sql: ${TABLE}.TLD ;;
    view_label: "Dimensions" #First dropdown
    label: "TLD"
    description: ""
    group_label: "Client"
    hidden: yes
  }
  dimension: ClientName {
    type: string
    sql: ${TABLE}.CLIENT_NAME ;;
    view_label: "Dimensions" #First dropdown
    label: "Client Name"
    description: ""
    group_label: "Client"
    hidden: yes
  }



  dimension: Manufacturer {
    type: string
    sql: ${TABLE}.MANUFACTURER_NAME ;;
    view_label: "Dimensions" #First dropdown
    label: "Manufacturer"
    description: ""
    group_label: "Catalog"
    hidden: yes
  }




  }
