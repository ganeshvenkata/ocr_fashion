view: catalog {

  label: "Catalog"
  sql_table_name: PUBLIC.Catalog ;;

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: ${Client_Id} || ${asin} ;;
  }


  dimension: Client_Id {
    type: number
    sql: ${TABLE}.Client_Id ;;
    hidden: yes
  }
  dimension_group: date_added {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.DATE_ADDED ;;
    view_label: "Dimensions" #First dropdown
    label: "Date Added - "
    description: ""
    group_label: "Time Frame"
  }
  dimension: asin {
    type: string
    sql: ${TABLE}.ASIN ;;
    view_label: "Dimensions" #First dropdown
    label: "ASIN"
    description: ""
    group_label: "Catalog"
    hidden: yes
  }
  dimension: title {
    type: string
    sql: ${TABLE}.Title ;;
    view_label: "Dimensions" #First dropdown
    label: "Title"
    description: ""
    group_label: "Catalog"
    hidden: yes
  }
  dimension: asin_title {
    type: string
    sql: ${asin} || ' - ' || ${title}  ;;
    view_label: "Dimensions" #First dropdown
    label: "ASIN Title"
    description: ""
    group_label: "Catalog"

  }
  dimension: brand {
    type: string
    sql: ${TABLE}.BRAND ;;
    view_label: "Dimensions" #First dropdown
    label: "Brand"
    description: ""
    group_label: "Catalog"
  }
  dimension: client_product_group {
    type: string
    sql: ${TABLE}.CLIENT_PRODUCT_GROUP ;;
    view_label: "Dimensions" #First dropdown
    label: "Client Product Group"
    description: ""
    group_label: "Catalog"
  }
  dimension: amazon_product_group {
    type: string
    sql: ${TABLE}.Amazon_PRODUCT_GROUP ;;
    view_label: "Dimensions" #First dropdown
    label: "Amazon Product Group"
    description: ""
    group_label: "Catalog"
  }
  dimension: amazon_category {
    type: string
    sql: ${TABLE}.AMAZON_CATEGORY ;;
    view_label: "Dimensions" #First dropdown
    label: "Amazon Category"
    description: ""
    group_label: "Catalog"
  }

  dimension: amazon_sub_category {
    type: string
    sql: ${TABLE}.AMAZON_SUB_CATEGORY ;;
    view_label: "Dimensions" #First dropdown
    label: "Amazon Sub Category"
    description: ""
    group_label: "Catalog"
  }
  dimension: category {
    type: string
    sql: ${TABLE}.CATEGORY ;;
    view_label: "Dimensions" #First dropdown
    label: "Category"
    description: ""
    group_label: "Catalog"
  }
  dimension: subcategory {
    type: string
    sql: ${TABLE}.SUBCATEGORY ;;
    view_label: "Dimensions" #First dropdown
    label: "Sub Category"
    description: ""
    group_label: "Catalog"
  }
  dimension: Manufacturer {
    type: string
    sql: ${TABLE}.Manufacturer ;;
    view_label: "Dimensions" #First dropdown
    label: "Manufacturer"
    description: ""
    group_label: "Catalog"
  }


  measure: count {
    type: count
    hidden: yes
    drill_fields: [brand, amazon_product_group, amazon_category, amazon_sub_category,category,subcategory]
  }



}
