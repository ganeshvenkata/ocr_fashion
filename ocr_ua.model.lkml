connection: "ocr_database"

# include all the views
include: "*.view"

named_value_format: mil {
  value_format: "#,##0,,\" M\""
}
week_start_day: sunday

named_value_format: bil {
  value_format: "#,##0,,,\" B\""
}

named_value_format: auto {
  value_format: "[>=1000000000]#,##0,,,\"bn\";[>=1000000]0,,\"mn\";[>=1000]#,##0;#"
}


datagroup: ocr_database_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: ocr_database_default_datagroup



explore: FashionData_Parent  {
  label: "Fashion Data (Parent ASIN)"
  from: catalog
  view_name: catalog
  fields: [
    dynamic_ranking_parent*,
    fashion_data.TotalPrice,
    fashion_data.TotalSales,
    fashion_data.TotalUnits,
    amazon_parent.week_beginning_date,
    amazon_parent.week_beginning_month,
    amazon_parent.week_beginning_quarter,
    amazon_parent.week_beginning_raw,
    amazon_parent.week_beginning_week,
    amazon_parent.week_beginning_year]
  always_join: [amazon_parent]
  join: client {
    sql_on: ${catalog.Client_Id}=${client.ClientId} ;;
    relationship: many_to_one
    type: inner
  }
  join: fashion_data {
    sql_on: ${fashion_data.ASIN}=${catalog.asin};;
    relationship: one_to_many
    type: inner
  }
  join: amazon_parent {
    sql_on:   ${amazon_parent.ASIN} = ${fashion_data.ASIN}
          and ${amazon_parent.week_beginning_raw} = ${fashion_data.week_beginning_raw}
          and ${amazon_parent.TLD}=${client.TLD};;
    relationship: many_to_one
    type: inner
  }
  join: dynamic_ranking_parent {

    sql_on:
      {% if dynamic_ranking_parent.dimension_name_selector._parameter_value == "'Manufacturer'" %} ${dynamic_ranking_parent.Dimension_name_null} = ${catalog.Manufacturer}
      {% elsif dynamic_ranking_parent.dimension_name_selector._parameter_value == "'Brand'" %} ${dynamic_ranking_parent.Dimension_name_null} = ${catalog.brand}
      {% elsif dynamic_ranking_parent.dimension_name_selector._parameter_value == "'Category'" %} ${dynamic_ranking_parent.Dimension_name_null} = ${catalog.category}
      {% elsif dynamic_ranking_parent.dimension_name_selector._parameter_value == "'Sub Category'" %} ${dynamic_ranking_parent.Dimension_name_null} = ${catalog.subcategory}
      {% elsif dynamic_ranking_parent.dimension_name_selector._parameter_value == "'Client Product Group'" %} ${dynamic_ranking_parent.Dimension_name_null} = ${catalog.client_product_group}
      {% elsif dynamic_ranking_parent.dimension_name_selector._parameter_value == "'ASIN'" %} ${dynamic_ranking_parent.Dimension_name_null} = ${amazon_parent.ASIN}
      {% elsif dynamic_ranking_parent.dimension_name_selector._parameter_value == "'ASIN Title'" %} ${dynamic_ranking_parent.Dimension_name_null} = ${catalog.asin_title}
      {% endif %};;
      relationship: many_to_one
    }
    sql_always_where:
         ${client.ClientId} = '660'
         and {% condition dynamic_ranking_parent.manufacturer_filter %} catalog.Manufacturer {% endcondition %}
         and {% condition dynamic_ranking_parent.brand_filter} %} catalog.brand {% endcondition %}
         and {% condition dynamic_ranking_parent.category_filter %} catalog.category {% endcondition %}
         and {% condition dynamic_ranking_parent.sub_category_filter %} catalog.subcategory {% endcondition %}
         and {% condition dynamic_ranking_parent.client_product_group_filter %} catalog.client_product_group {% endcondition %}
         and {% condition dynamic_ranking_parent.parent_asin_filter %} amazon_parent.ASIN {% endcondition %}
         and {% condition dynamic_ranking_parent.asin_title_filter %} ${catalog.asin_title} {% endcondition %}
         and {% if dynamic_ranking_parent.show_all_others._parameter_value == "'No'" %} ${dynamic_ranking_parent.Dimension_name}!='-All Others-' and
            {%endif%}
         1=1
        ;;
    }



explore: FashionData_Child  {
  label: "Fashion Data (Child ASIN)"
  from: catalog
  view_name: catalog
  fields: [dynamic_ranking_child*,
    fashion_data.Total1PPrice,
    fashion_data.Total1PSales,
    fashion_data.Total1PUnits,
    fashion_data.Total3PSales,
    fashion_data.Total3PUnits,
    fashion_data.Total3PPrice,

    amazon_child.week_beginning_date,
     amazon_child.week_beginning_month,
     amazon_child.week_beginning_quarter,
     amazon_child.week_beginning_raw,
     amazon_child.week_beginning_week,
     amazon_child.week_beginning_year]
  always_join: [amazon_child]
  join: client {
    sql_on: ${catalog.Client_Id}=${client.ClientId} ;;
    relationship: many_to_one
    type: inner
  }
  join: fashion_data {
    sql_on: ${fashion_data.ASIN}=${catalog.asin};;
    relationship: one_to_many
    type: inner

  }
  join: amazon_child {

    sql_on:   ${amazon_child.ASIN} = ${fashion_data.ASIN}
          and ${amazon_child.week_beginning_raw} = ${fashion_data.week_beginning_raw}
          and ${amazon_child.TLD}=${client.TLD}
          and ${client.Manufacturer}=${catalog.Manufacturer};;
    relationship: many_to_one
    type: inner
  }
  join: dynamic_ranking_child {
    sql_on:
      {% if dynamic_ranking_child.dimension_name_selector._parameter_value == "'Manufacturer'" %} ${dynamic_ranking_child.Dimension_name} = ${catalog.Manufacturer}
      {% elsif dynamic_ranking_child.dimension_name_selector._parameter_value == "'Brand'" %} ${dynamic_ranking_child.Dimension_name} = ${catalog.brand}
      {% elsif dynamic_ranking_child.dimension_name_selector._parameter_value == "'Category'" %} ${dynamic_ranking_child.Dimension_name} = ${catalog.category}
      {% elsif dynamic_ranking_child.dimension_name_selector._parameter_value == "'Sub Category'" %} ${dynamic_ranking_child.Dimension_name} = ${catalog.subcategory}
      {% elsif dynamic_ranking_child.dimension_name_selector._parameter_value == "'Client Product Group'" %} ${dynamic_ranking_child.Dimension_name} = ${catalog.client_product_group}
      {% elsif dynamic_ranking_child.dimension_name_selector._parameter_value == "'ASIN'" %} ${dynamic_ranking_child.Dimension_name} = ${amazon_child.ASIN}
      {% elsif dynamic_ranking_child.dimension_name_selector._parameter_value == "'ASIN Title'" %} ${dynamic_ranking_child.Dimension_name} = ${catalog.asin_title}
      {% endif %};;
    relationship: many_to_one
  }
  sql_always_where:
    ${client.ClientId} = '660'
         and {% condition dynamic_ranking_child.manufacturer_filter %} catalog.Manufacturer {% endcondition %}
         and {% condition dynamic_ranking_child.brand_filter} %} catalog.brand {% endcondition %}
         and {% condition dynamic_ranking_child.category_filter %} catalog.category {% endcondition %}
         and {% condition dynamic_ranking_child.sub_category_filter %} catalog.subcategory {% endcondition %}
         and {% condition dynamic_ranking_child.client_product_group_filter %} catalog.client_product_group {% endcondition %}
         and {% condition dynamic_ranking_child.child_asin_filter %} amazon_child.ASIN {% endcondition %}
         and {% condition dynamic_ranking_child.asin_title_filter %} ${catalog.asin_title} {% endcondition %}
         and {% if dynamic_ranking_child.show_all_others._parameter_value == "'No'" %} ${dynamic_ranking_child.Dimension_name}!='-All Others-' and
            {%endif%}
         1=1
        ;;
}
explore: manufacturer_lookups {
  hidden: yes
}
explore: brand_lookups  {
  hidden: yes
}
explore: category_lookups {
  hidden: yes
}
explore: sub_category_lookups {
  hidden: yes
}
explore: client_product_group_lookups {
  hidden: yes
}
